import win32com.client
import sys
import os
import shutil
import time

FOLDER_SOURCE = "C:\\Users\\Freeman\\Desktop\\cec verix\\new\\"
FOLDER_DISTINATION = "C:\\Users\\Freeman\\Desktop\\cec verix\\new\\out\\"
COMPORT = 3
COMSPEED = 9600

class FileCec():
    def __init__(self, filepath):
        self.selldata = [] # список словарей содержащий распарсеный текст чека
        self.payments = {'cash':0, 'card':0}
        self.withdraw = {'sum':0}
        self.error = False
        self.other_operation = False # все операции, кроме печати чека продажи
        self.filepath = filepath
        self.parse_file()

        if not self.other_operation: # если это не прочие опрации, тогда печатаем чек
            self.procesing_data()
            self.print_cec()

    def procesing_data(self):
        if not self.error:
            try:
                self.create_discount()
                self.typecasting()
            except:
                self.error = True
                print(sys.exc_info()[0])
        else:
            print("Произошла ошибка")

    def parse_file(self):
        with open(self.filepath, "r") as f:
            try :
                for line in f:
                    if not self.other_operation:
                        cec_line = self.execute_text_to_dict(line)
                        if cec_line["type"] == "":
                            self.error = True
                            raise IOError

                        elif cec_line["type"] == "payment" and cec_line["cash"] > 0:
                            self.payments["cash"] = cec_line["cash"]
                        elif cec_line["type"] == "payment" and cec_line["card"] > 0:
                            self.payments["card"] = cec_line["card"]

                        else:
                            self.selldata.append(cec_line)
            except:
                self.error = True
                print(sys.exc_info()[0])

    def execute_text_to_dict(self, line):
        str_list = line.split(";")
        cec_line = {'type':"", 'name':"", 'price':0, 'amount':0, 'department':1, 'tva':0, 'discount':0, 'cash':0, 'card':0, 'text':"",  }

        # продажа
        if str_list[0] == "S,1,______,_,__":
            cec_line["type"]           = 'sell'
            cec_line["name"]           = str_list[1]
            cec_line["price"]          = str_list[2]
            cec_line["amount"]         = str_list[3]
            cec_line["department"]     = str_list[4]
            cec_line["tva"]            = str_list[5]


        #скидка
        elif str_list[0] == "C,1,______,_,__":
            cec_line["type"]           = 'discount'
            cec_line["discount"]       = str_list[2]


        # произвольный текст
        elif str_list[0] == "P,1,______,_,__":
            # наличная
            cec_line["type"]           = 'simpletext'
            cec_line["text"]           = str_list[1]

        # оплата
        elif str_list[0] == "T,1,______,_,__":
            # наличная
            cec_line["type"]           = 'payment'
            if str_list[1] == '0' and str_list[2]:
                cec_line.update({'cash': float(str_list[2])})
            elif str_list[1] == '3' and str_list[2]:
                cec_line.update({'card': float(str_list[2])})

        # изъятие денег
        elif str_list[0] == "I,1,______,_,__":
            self.other_operation = True
            cec_line["type"] = 'withdraw'

            if str_list[1] == '0':
                self.withdraw["sum"]       = float(str_list[2])
            else:
                self.withdraw["sum"]       = float(str_list[2])*-1

            self.print_withdraw()

        elif str_list[0] == '0':
            self.other_operation = True
            cec_line["type"] = 'anulat'
            self.anulat_cec()

        return  cec_line

    def create_discount(self):
        """ Скидки установленные отдельной строкой, запишем в строку продажи"""
        del_list = []
        index = 0
        for x in self.selldata:
            if x['type'] == 'discount':
                #получил индекc предыдушей строки и запишем туда скидку
                index_preview = index - 1
                self.selldata[index_preview]['discount'] = x['discount']
                del_list.append(x)

            index = index + 1

        for x_remove in range(len(del_list)):
            self.selldata.remove(del_list[x_remove])

    def typecasting(self):
        """Приведение типов для начала печати чека"""
        for x in self.selldata:
            if x['type'] == 'sell':
                x["price"]          = float(x["price"])
                x["amount"]         = float(x["amount"])
                x["department"]     = int(x["department"])
                x["tva"]            = int(x["tva"])
                x["discount"]       = float(x["discount"])*-1

    def print_cec(self):
        if not self.error:
            zekadriver = Zeka()

            if self.selldata:
                zekadriver.open_bon()

                for data in self.selldata:
                    zekadriver.print_dict_data(data)

                zekadriver.close_bon(self.payments)
            # перенесем ошибку из класса печати в текущий
            self.error = zekadriver.error

    def print_withdraw(self):
        if self.withdraw['sum'] != 0:
            sum_withdraw = self.withdraw['sum']
            if not self.error:
                zekadriver = Zeka()
                zekadriver.withdraw_cec(sum_withdraw)

    def anulat_cec(self):
        zekadriver = Zeka()
        zekadriver.terminate_bon()

class Zeka():
    def __init__(self):
        self.com = COMPORT
        self.comspeed = COMSPEED
        self.obj = win32com.client.Dispatch("Zfpcom.ZekaFP")
        self.setup_driver()
        self.error = False

    def check_error(self):
        if self.obj.ErrorCode != 0:
            codError = self.obj.ErrorCode
            errtext = self.obj.GetErrorString(codError, 0)
            self.error = True
            print(errtext)

            if codError == 178:
                print("**************************************   Необходимо закрыть Z-отчет   *******************************************")
                self.raport_Z()
                self.error = False

    def setup_driver(self):
       req = self.obj.Setup(self.com, self.comspeed, 3, 1000)

    def open_bon(self):
        if not self.error:
            passwd = '0000'
            req = self.obj.OpenFiscalBon( 1, passwd, 0, 0 )
            self.check_error()

    def print_dict_data(self, dict_data):
        if not self.error:
            if dict_data['type'] == 'sell':
                req = self.obj.SellFree( dict_data['name'], dict_data['tva'], dict_data['price'], dict_data['amount'], dict_data['discount'] )
                self.check_error()
            elif dict_data['type'] == 'simpletext':
                req = self.obj.PrintText(dict_data['text'], 0)
                self.check_error()

    def close_bon(self, payments):
        if not self.error:
            #вносим оплаты
            if payments["card"] > 0:
                req = self.obj.Payment(payments["card"], 3, False)

            if payments["cash"] > 0:
                req = self.obj.Payment(payments["cash"], 0, False)

            self.check_error()

            # если нет ошибок, то без закрывам
            if not self.error:
                req = self.obj.CloseFiscalBon()
                self.check_error()

    def terminate_bon(self):
        req = self.obj.TerminateBon(False)

    def withdraw_cec(self, sum_withdraw):
        passwd = '0000'
        req = self.obj.OfficialSums(1, passwd, 0, sum_withdraw)
        self.check_error()

    def raport_Z(self):
        req = self.obj.ReportDaily(True, False)

    def raport_x(self):
        req = self.obj.ReportDaily(False, False)

def move_file(file):
    shutil.move(FOLDER_SOURCE+file, FOLDER_DISTINATION+file)


if __name__=="__main__":
    while True:
        for file in os.listdir(FOLDER_SOURCE):
            if file.endswith(".txt"):
                time.sleep(1)
                try:
                    if file == 'Z_raport.txt':
                        Zeka().raport_Z()
                        move_file(file)
                    elif file == 'X_raport.txt':
                        Zeka().raport_x()
                        move_file(file)
                    else:
                        #Zeka().terminate_bon()
                        cec = FileCec(FOLDER_SOURCE+file)
                        if not cec.error:
                            move_file(file)
                        else:
                            print("  \r\n"
                                  "   Ошибка при печати файла " + str(file))
                            exit()

                except PermissionError:
                    print("Терпение!!!!!")
                    pass